#pragma once

#include <vector>
// #include <fstream>
#include "CheckMatrix.h"

using namespace std;

void print_matrix(vector<vector<int>> matrix)
{
    cout << endl;
    for (int r = 0; r < matrix.size(); r++)
    {
        for (int c = 0; c < matrix[r].size(); c++)
        {
            cout << matrix[r][c] << " ";
            // cout << matrix[r][c] << ":" << ((r / 3) * 3 + c / 3) << " ";
        }
        cout << endl;
    }
}

vector<vector<int>> gen_matrix_permutation(vector<vector<int>> matrix, int rPos, int cPos, vector<vector<bool>> used,
                                           vector<int> maskRows, vector<int> maskColumns, vector<int> maskSquares)
{
    vector<vector<int>> matrixResult;

    int size = matrix.size();

    if (cPos == size)
    {
        cPos = 0;
        rPos++;
    }
    if (rPos == size)
    {
        print_matrix(matrix);
        cout << "check_matrix: " << check_matrix(matrix, matrix) << endl;
        return matrix;
    }
    else
    {
        if (matrix[rPos][cPos] > 0)
        {
            return gen_matrix_permutation(matrix, rPos, cPos + 1, used, maskRows, maskColumns, maskSquares);
        }

        for (int c = 0; c < size; c++)
        {
            int squareIdx = (rPos / 3) * 3 + cPos / 3;
            int value = c + 1;
            bool isNumberRepeatedInRows = (maskRows[rPos] & (1 << value)) > 0;
            bool isNumberRepeatedInColumn = (maskColumns[cPos] & (1 << value)) > 0;
            bool isNumberRepeatedInSquare = (maskSquares[squareIdx] & (1 << value)) > 0;
            bool isUsed = used[rPos][c];

            if (!isUsed && !isNumberRepeatedInRows && !isNumberRepeatedInColumn && !isNumberRepeatedInSquare)
            {
                used[rPos][c] = true;

                int saveMaskRows = maskRows[rPos];
                maskRows[rPos] |= 1 << value;

                int saveMaskColumns = maskColumns[cPos];
                maskColumns[cPos] |= 1 << value;

                int saveMaskSquares = maskSquares[squareIdx];
                maskSquares[squareIdx] |= 1 << value;

                matrix[rPos][cPos] = value;

                matrixResult = gen_matrix_permutation(matrix, rPos, cPos + 1, used, maskRows, maskColumns, maskSquares);
                if (matrixResult.size())
                {
                    return matrixResult;
                }

                used[rPos][c] = false;
                maskRows[rPos] = saveMaskRows;
                maskColumns[cPos] = saveMaskColumns;
                maskSquares[squareIdx] = saveMaskSquares;
            }
        }
    }

    return matrixResult;
}

vector<vector<int>> sudoku(vector<vector<int>> matrix)
{
    int size = matrix.size();

    vector<vector<bool>> matrixUsed;
    vector<int> maskRows;
    vector<int> maskColumns;
    vector<int> maskSquares;
    for (int i = 0; i < size; i++)
    {
        maskRows.push_back(0);
        maskColumns.push_back(0);
        maskSquares.push_back(0);
    }

    for (int r = 0; r < size; r++)
    {
        vector<int> matrixRow;
        vector<bool> matrixUsedRow;
        for (int c = 0; c < size; c++)
        {
            int value = matrix[r][c];
            if (value > 0)
            {
                int squareIdx = (r / 3) * 3 + c / 3;
                maskRows[r] |= 1 << value;
                maskColumns[c] |= 1 << value;
                maskSquares[squareIdx] |= 1 << value;
            }

            matrixRow.push_back(0);
            matrixUsedRow.push_back(false);
        }
        matrixUsed.push_back(matrixUsedRow);
    }

    print_matrix(matrix);

    return gen_matrix_permutation(matrix, 0, 0, matrixUsed, maskRows, maskColumns, maskSquares);
}
