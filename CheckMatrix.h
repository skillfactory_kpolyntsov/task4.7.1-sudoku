#pragma once

#include <vector>
#include <fstream>

using namespace std;

bool check_matrix(vector<vector<int>> matrix, vector<vector<int>> matrixCopy)
{
    int maskRows[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    int maskColumns[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    int maskSquares[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            int squareIdx = (r / 3) * 3 + c / 3;
            bool ifMatrixTaskMapRewritten = matrixCopy[r][c] != 0 && matrixCopy[r][c] != matrix[r][c];
            bool ifMatrixHasZeroNumber = matrix[r][c] == 0;
            bool ifNumberRepeatedInRow = (maskRows[r] & (1 << matrix[r][c])) > 0;
            bool ifNumberRepeatedInColumn = (maskColumns[c] & (1 << matrix[r][c])) > 0;
            bool ifNumberRepeatedInSquare = (maskSquares[squareIdx] & (1 << matrix[r][c])) > 0;

            maskRows[r] |= 1 << matrix[r][c];
            maskColumns[c] |= 1 << matrix[r][c];
            maskSquares[squareIdx] |= 1 << matrix[r][c];

            if (ifMatrixTaskMapRewritten || ifMatrixHasZeroNumber || ifNumberRepeatedInRow || ifNumberRepeatedInColumn)
            {
                return false;
            }
        }
    }

    return true;
}
